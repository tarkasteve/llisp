from __future__ import print_function

import os, sys, re, logging
from lisp import Cell, String, Boolean, LTrue, LFalse, Symbol, Integer, cell_p

######################################################################
# Wrapper string or file ops

class Input(object):
    def getchar(self):
        raise NotImplementedError
    def back(self):
        raise NotImplementedError


class FileInput(Input, file):
    def __init__(self, fname):
        self._fd = open(fname, 'r')

    def getchar(self):
        char = self._fd.read(1)
        if char == '':
            return None
        if char != ' ' and char != '\n':
            logging.debug("Char: '%s'"%char)
        return char

    def back(self):
        self._fd.seek(-1, os.SEEK_CUR)
        return self


class StringInput(Input):
    def __init__(self, str):
        self.str = str
        self.idx = 0

    def getchar(self):
        if self.idx >= len(self.str):
            return None
        char = self.str[self.idx]
        self.idx += 1
        return char

    def back(self):
        self.idx -= 1
        return self
    

######################################################################
# Lexer/Parser

symre = re.compile('[<>0-9A-Za-z_+=\-!@*]')
def symchar(char):
    if char == None:
        return False
    return symre.match(char) != None

numre = re.compile('^[0-9]+$')
def isnum(tok):
    return numre.match(tok) != None

boolre = re.compile('^\#[tf]$')
def isbool(tok):
    return boolre.match(tok) != None

def readlist(fd, depth):
    # StartList
    next = nextexpr(fd, depth)
    retval = Cell(car=next)
    end = retval
    while next != None:
        next = nextexpr(fd, depth)
        if next != None:
            end._cdr = Cell(car=next)
            end = end._cdr
    return retval


# FIXME: Should be function accepting tok?
sharptable = {'t':LTrue,
              'f':LFalse}
def readsharp(fd, depth):
    char = fd.getchar()
    try:
        return sharptable[char]
    except KeyError:
        raise SyntaxError


def readsym(fd, depth):
    fd.back()
    tok = ''
    char = fd.getchar()

    if not symchar(char):
        raise SyntaxError("Unknown char: %s"%char)        

    while symchar(char):
        tok += char
        char = fd.getchar()
    if char != None:
        fd.back()

    if isnum(tok):
        return Integer(int(tok))
    else:
        return Symbol(tok)


def readstr(fd, depth):
    tok = ''
    char = fd.getchar()
    while char != '"':
        tok += char
        char = fd.getchar()
    return String(tok)


def quoteexpr(fd, depth):
    exp = nextexpr(fd, depth)
    return Cell(Symbol("quote"), Cell(exp))


def backquoteexpr(fd, depth):
    exp = nextexpr(fd, depth)
    return Cell(Symbol("backquote"), Cell(exp))    

def unquoteexpr(fd, depth):
    exp = nextexpr(fd, depth)
    return Cell(Symbol("unquote"), Cell(exp))    

def uquotesplice(fd, depth):
    exp = nextexpr(fd, depth)
    return Cell(Symbol("uq-splice"), Cell(exp))    


readtable = {'(':readlist,
             ')':lambda fd, depth: None,
             '#':readsharp,
             '"':readstr,
             "'":quoteexpr,
             "`":backquoteexpr,
             "@":uquotesplice,
             ",":unquoteexpr}

def nextexpr(fd, depth=1):

    char = fd.getchar()
    while char != None and char.isspace():
        char = fd.getchar()

    retval = None
    if char in readtable:
        retval = readtable[char](fd, depth)
    elif char == None:
        pass # End of input
    else:  # Assume symbol/int/etc.
        retval = readsym(fd, depth)

    logging.debug("%s %s" % ('>' * depth, retval.__class__))
    return retval


def parsestring(str):
    return nextexpr(StringInput(str))

def parsefile(fname):
    return parseall(FileInput(fname))


def print_ast(node, indent=0):
    if cell_p(node):
        print(' ' * indent, node.car.__class__)
        if node.cdr != None:
            print_ast(node.cdr, indent+3)
    else:
        print(' ' * indent, node.__class__, "=", node.val)


