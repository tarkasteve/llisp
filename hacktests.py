#!/usr/bin/python

import logging, unittest
from parser import parsestring, StringInput, print_ast
from lisp import Integer, Cell, Symbol, String, Boolean, print_rep, str_rep
from lisp import int_p, atom_p, str_p, cell_p, sym_p, evaluate, extend, lookup, primitives

# (defun twox (x) (+ x x))")
twox = Cell(Symbol("defun"),
            Cell(Symbol("twox"),
                 Cell(Cell(Symbol("x")),
                      Cell(Cell(Symbol("+"),
                                Cell(Symbol("x"),
                                     Cell(Symbol("x"))))))))
twolists = Cell(Cell(Symbol("x")), Cell(Cell(Symbol("y"))))
dotpair = Cell(Symbol("x"), Symbol("y"))
ifexp = Cell(Symbol("if"), 
             Cell(Boolean("#t"),
                  Cell(Integer(1),
                       (Cell(Integer(2))))))

print_rep(twox)
print_rep(twolists)
print_rep(dotpair)

print_rep(Cell())


print parsestring("#t")

e = parsestring("(if x 1 2)")
print_rep(e)
print e.caddr
print e.cadddr

print twox.caddr.car

e = parsestring('(begin 2)')
print(evaluate(e,None))
print_rep(evaluate(e,None))

env = extend(None, Symbol('x'), Integer(5))
print_rep(env)
print(lookup(Symbol("x"), env))
print(evaluate(parsestring('x'), extend(None, Symbol('x'), Integer(5))))

print("====================")
vars = Cell(Symbol("x"), Cell(Symbol("y")))
vals = Cell(Integer(5), Cell(Integer(10)))
env = extend(None, vars, vals)
ret = evaluate(parsestring('y'), env)
print_rep(ret)

print("====================")
# exp = """(((lambda (a)
#         	    (lambda (b)
#         	     ((lambda (vals) vals)) a b)) 1) x)"""
# sexp = parsestring(exp)

# print_rep(sexp)


# env = extend(None, Symbol('x'), Integer(5))
# ret = evaluate(sexp, env)

print("====================")
env = extend(None, Symbol('f'), Integer(0))
#ret = evaluate(parsestring('(set! f (lambda (x) x))'), env)
ret = evaluate(parsestring('(begin (set! f (lambda (x) x)) (f 5))'), env)

print("====================")
env = primitives()
env = extend(env, Symbol('x'), Integer(5))
ret = evaluate(parsestring('(+ x x)'), env)
print(ret)



env = primitives()
env = extend(env, Symbol('y'), 
             evaluate(parsestring('(quote (a b c))'), env))
exp = '`(y ,y ,@y foo ,(cadr y) bar ,(cdr y) baz ,@(cdr y))'
ret = parsestring(exp)
logging.root.setLevel(logging.DEBUG)
ret = evaluate(ret, env)
print_rep(ret)
