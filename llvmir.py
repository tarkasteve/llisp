from __future__ import print_function

import logging
from parser import *


def out(lines):
    print(lines)


class Defun(object):
    def __enter__(self):
        pass

    def __exit__(self, type, value, traceback):
        out('}\n')

def defun(name, ret_t):
    out('define %s @%s() {' % (ret_t, name))
    out('entry:')
    return Defun()


def bootstrap(modname):
    out("; ModuleID = '%s'\n" % modname)

def gen(ast):
    bootstrap("llisp")

    rgen(ast)


def rgen(ast):
    # Special forms
    next = ast.list[0]
    if isinstance(next, Symbol) and next.val == "defun":
        gen_defun(ast)

    else:
        for node in ast.list:
            if isinstance(node, List):
                #print(node.__class__)
                rgen(node)

            else:
                pass
                #print(node.__class__, "=", node.val)


def gen_defun(ast):
    with defun(ast.list[1].val, 'i32'):
        pass
