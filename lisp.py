from __future__ import print_function

import logging


######################################################################
# Core type definitions and operations

class Cell(object):
    def __init__(self, car=None, cdr=None):
        self._car=car
        self._cdr=cdr

    @property
    def car(self):
        return self._car

    @property
    def cdr(self):
        return self._cdr

    @property
    def cadr(self):
        return self.cdr.car

    @property
    def cdar(self):
        return self.car.cdr

    @property
    def cddr(self):
        return self.cdr.cdr

    @property
    def caar(self):
        return self.car.car

    @property
    def caadr(self):
        return self.cdr.car.car

    @property
    def caaddr(self):
        return self.cdr.cdr.car.car

    @property
    def caddr(self):
        return self.cdr.cdr.car

    @property
    def cadddr(self):
        return self.cdr.cdr.cdr.car

        

class Atom(object):
    def __init__(self, val):
        self._val = val

    @property
    def val(self):
        return self._val

    def __str__(self):
        return str(self._val)

class Boolean(Atom):
    def __init__(self, val):
        Atom.__init__(self, val)

    @property
    def val(self):
        return self._val

LTrue = Boolean(True)
LFalse = Boolean(False)

class Symbol(Atom):
    def __init__(self, val):
        Atom.__init__(self, val)

class Integer(Atom):
    def __init__(self, val):
        Atom.__init__(self, val)

    def sum(self, rhs):
        return Integer(self.val + rhs.val)

    def mult(self, rhs):
        return Integer(self.val * rhs.val)

    def subtract(self, rhs):
        return Integer(self.val - rhs.val)

class String(Atom):
    def __init__(self, val):
        Atom.__init__(self, val)

    def __str__(self):
        return '"'+str(self._val)+'"'


class Invokable(object):
    def invoke(self, args, env):
        raise NotImplementedError("Invoke called on base class")        

class Lambda(Invokable):
    def __init__(self, vars, body, env):
        logging.debug("LAMBDA: %s: %s" % (str_rep(vars), str_rep(body)))
        self.vars = vars
        self.body = Cell(Symbol("begin"), body)
        self.env = env

    def invoke(self, args, env):
        ret = evaluate(self.body, extend(self.env, self.vars, evallist(args, env)))
        logging.debug("Invoke: %s: %s -> %s" % (str_rep(self.vars), 
                                                str_rep(self.body),
                                                str_rep(ret)))
        return ret

class Primitive(Invokable):
    def __init__(self, op):
        self.op = op

    def invoke(self, args, env):
        return self.op(evallist(args, env))

class Macro(Invokable):
    def __init__(self, vars, body, env):
        self.op = op

    def invoke(self, args, env):
        return self.op(evallist(args, env))


def atom_p(v): return isinstance(v, Atom)
def bool_p(v): return isinstance(v, Boolean)
def int_p(v): return isinstance(v, Integer)
def sym_p(v): return isinstance(v, Symbol)
def str_p(v): return isinstance(v, String)
def cell_p(v): return isinstance(v, Cell)
def fun_p(v): return isinstance(v, Invokable)


######################################################################

def evallist(exp, env):
    logging.debug("EVALLIST: %s"%str_rep(exp))    
    if cell_p(exp):
        return Cell(evaluate(exp.car, env),
                    evallist(exp.cdr, env))
    else:
        return None


# Certain special forms modify the environment and can only be called
# at the top level.  These return the value and the new environment.
def tleval(exp, env):
    if cell_p(exp) and exp.car.val == 'defun':
        return defun(exp.cdr, env)
    elif cell_p(exp) and exp.car.val == 'define':
        return define(exp.cdr, env)
    else:
        return evaluate(exp, env), env


def evaluate(exp, env):
    logging.debug("evaluating: %s"%str_rep(exp))

    # Self-evaluating atoms
    if int_p(exp) or str_p(exp) or bool_p(exp):
        return exp

    elif sym_p(exp):
        return lookup(exp, env)

    elif cell_p(exp):
        car = exp.car
        if sym_p(car):
            if car.val == 'quote':
                return exp.cadr

            if car.val == 'backquote':
                return backquote(exp.cadr, env)

            elif car.val == 'if':
                ret = evaluate(exp.cadr, env)
                if bool_p(ret) and ret.val:
                    return evaluate(exp.caddr, env)
                else:
                    return evaluate(exp.cadddr, env)

            elif car.val == 'begin':
                return eprogn(exp.cdr, env)

            elif car.val == 'set!':
                return update(exp.cadr, evaluate(exp.caddr, env), env)

            elif car.val == "lambda":
                return Lambda(exp.cadr, exp.cddr, env)

            elif car.val == "defun":
                raise ValueError("'defun' can only be invoked at the top level")
            elif car.val == "define":
                raise ValueError("'define' can only be invoked at the top level")

        # Default operation is invoke the car of the cell
        car = evaluate(car, env)
        if fun_p(car):
            return car.invoke(exp.cdr, env)
        else:
            raise ValueError("Non-function in functional position: %s" % str_rep(car))

    else:
        raise NotImplementedError("Unknown type: %s"%exp.__class__)



def defun(args, env):
    name = args.car
    arglist = args.cadr
    body = args.cddr
    # Push the name into the environment so the lambda closure can see
    # it.  This is necessary for recursion.
    env = extend(env, name, None)
    fun = Lambda(arglist, body, env)
    update(name, fun, env)
    return None, env

def define(args, env):
    name = args.car
    val = evaluate(args.cadr, env)
    # Push the name into the environment so the lambda closure can see
    # it.  This is necessary for recursion.
    env = extend(env, name, val)
    return val, env


def eprogn(exp, env):
    if not cell_p(exp):
        return Cell() # Empty
    else:
        if cell_p(exp.cdr): # More than one; eval first, recurse over rest
            evaluate(exp.car, env)
            return eprogn(exp.cdr, env)

        else:            # Final; evaluate to return value            
            return evaluate(exp.car, env)


def backquote(exp, env):
    if exp == None:
        return exp

    if cell_p(exp):
        if sym_p(exp.car) and exp.car.val == "unquote":
            return evaluate(exp.cadr, env)

        # Lookahead for splice
        elif cell_p(exp.car) and exp.caar.val == "uq-splice":
            e = evaluate(exp.car.cdr.car, env)
            if not cell_p(e):
                raise SyntaxError("We don't support non-list splices currently")

            def copysplice(lst, end):
                if lst == None:
                    return end
                else:
                    return Cell(lst.car, copysplice(lst.cdr, end))
            return copysplice(e, backquote(exp.cdr, env))

        else:
            return Cell(backquote(exp.car, env), 
                        backquote(exp.cdr, env))
    else:
        return exp


def extend(env, vars, vals):
    logging.debug("Extend: %s %s"%(str_rep(vars),str_rep(vals)))
    if cell_p(vars) and vars.car != None:
        if cell_p(vals) and vals.car != None:
            return Cell(Cell(vars.car.val, vals.car),
                        extend(env, vars.cdr, vals.cdr))
        else:
            raise ValueError("Not enough values in extend")

    elif sym_p(vars):  # Single symbol
        return Cell(Cell(vars.val, vals), env)

    elif vars == None:
        if vals == None:
            return env  # Done
        else:
            raise ValueError("Too many values in extend")

    else:
        # NOOP
        #raise TypeError("Unknown value in vars: %s"%vars)
        return env


def update(id, val, env):
    if env == None or not cell_p(env):
        raise LookupError("Not Found: %s"%id)

    if env.caar == id.val:
        env.car._cdr = val
        return val
    else:
        return update(id, val, env.cdr)


def lookup(id, env):
    if not cell_p(env):
        raise LookupError("Symbol Not Found: %s"%id)
    
    if env.caar == id.val:
        return env.cdar
    else:
        return lookup(id, env.cdr)


def equiv(a, b):
    if type(a) != type(b):
        return False
    elif cell_p(a) and cell_p(b):
        if a.cdr == None and b.cdr == None:
            return True # End
        else:
            return equiv(a.car, b.car) and equiv(a.cdr, b.cdr)
    elif a.val == b.val:
        return True
    return False # NFI


def str_rep(node, iscar=False, depth=0):
    if node == None:
        return ''
    elif cell_p(node):
        ret = str_rep(node.car, iscar=True, depth=depth+1)
        ret += ' ' 
        if not cell_p(node.cdr) and node.cdr != None:
            ret += '. '
        ret += str_rep(node.cdr, depth=depth+1) 
        if iscar or depth == 0:
            return '( ' + ret + ' )'
        return ret
    else:
        return str(node)


def print_rep(node):
    print(str_rep(node))
    print


######################################################################
# Primitives

def do_lt(args):
    if not int_p(args.car) or not int_p(args.cadr):
        raise ValueError("Non-Int passed to lt primitive")
    return Boolean(args.car.val < args.cadr.val)

def do_equal(args):
    # FIXME
    if not int_p(args.car) or not int_p(args.cadr):
        raise ValueError("Non-Int passed to equal primitive")
    return Boolean(args.car.val == args.cadr.val)

def do_sum(args):
    if not cell_p(args):
        return Integer(0);

    car = args.car
    if not int_p(car):
        raise ValueError("Non-Int passed to sum primitive")

    return car.sum(do_sum(args.cdr))

def do_mult(args):
    if not cell_p(args):
        return Integer(1)  # Implicit * 1

    car = args.car
    if not int_p(car):
        raise ValueError("Non-Int passed to mult primitive")

    return car.mult(do_mult(args.cdr))


# FIXME: Implements > 2 args
def do_subtract(args):
    return args.car.subtract(args.cadr)

def defprimitive(sym, op, env):
    return extend(env, Symbol(sym), Primitive(op))


def primitives(env=None):
    env = defprimitive('list', lambda args: args, env)
    env = defprimitive('car', lambda args: args.car.car, env)
    env = defprimitive('cdr', lambda args: args.car.cdr, env)
    env = defprimitive('cadr', lambda args: args.car.cadr, env)
    env = defprimitive('caar', lambda args: args.car.caar, env)
    env = defprimitive('-', do_subtract, env)
    env = defprimitive('+', do_sum, env)
    env = defprimitive('*', do_mult, env)
    env = defprimitive('<', do_lt, env)
    env = defprimitive('=', do_equal, env)
    return env


######################################################################

# if __name__ == '__main__':
#     from parser import parsestring
#     env = primitives()
#     quit = False
#     while not quit:
#         try:
#             line = raw_input("> ")
#         except EOFError:
#             self.stdout.write("\n")
#             break
#         if not line:
#             continue

#         print(line)

#         try:
#             ret, env = evaluate(parsestring(line), env)
#             print_rep(ret)
#         except:
#             import traceback, sys
#             traceback.print_exc(sys.stderr)
