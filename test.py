#!/usr/bin/python

import logging, unittest
from parser import parsestring, StringInput, print_ast
import lisp
from lisp import Integer, Cell, Symbol, String
from lisp import int_p, atom_p, bool_p, str_p, cell_p, sym_p, fun_p
from lisp import evaluate, tleval, extend, lookup, equiv, print_rep, primitives


class TestLispOps(unittest.TestCase):

    def setUp(self):
        # (defun twox (x) (+ x x))")
        self.twox = Cell(Symbol("defun"),
                         Cell(Symbol("twox"),
                              Cell(Cell(Symbol("x")),
                                   Cell(Cell(Symbol("+"),
                                             Cell(Symbol("x"),
                                                  Cell(Symbol("x"))))))))
        self.twolists = Cell(Cell(Symbol("x")), Cell(Cell(Symbol("y"))))
        self.dotpair = Cell(Symbol("x"), Symbol("y"))

    def testCellCarCdrTypes(self):
        self.assertTrue(cell_p(self.twox))
        self.assertTrue(sym_p(self.twox.car))
        self.assertTrue(cell_p(self.twox.cdr))

    def testCellCar(self):
        self.assertEqual(self.twox.car.val, "defun")

    def testCellCadr(self):
        self.assertEqual(self.twox.cadr.val, "twox")

    def testCellCaddr(self):
        self.assertTrue(cell_p(self.twox.caddr))
        self.assertEqual(self.twox.caddr.car.val, 'x')

    def testCellCadddr(self):
        self.assertTrue(cell_p(self.twox.caddr))
        self.assertEqual(self.twox.cadddr.car.val, '+')

    def testCellCaaddr(self):
        self.assertEqual(self.twox.caaddr.val, "x")

    def testTwoLists(self):
        self.assertEqual(self.twolists.caar.val, "x")
        self.assertEqual(self.twolists.caadr.val, "y")

    def testDotPair(self):
        self.assertEqual(self.dotpair.car.val, "x")
        self.assertEqual(self.dotpair.cdr.val, "y")


class TestParser(unittest.TestCase):
    
    def testIntAtom(self):
        ast = parsestring("1")
        self.assertTrue(int_p(ast))
        self.assertEqual(ast.val, 1)

    def testStringAtom(self):
        ast = parsestring('"wibble"')
        self.assertTrue(str_p(ast))
        self.assertEqual(ast.val, "wibble")

    def testSymAtom(self):
        ast = parsestring('wibble')
        self.assertTrue(sym_p(ast))
        self.assertEqual(ast.val, "wibble")

    def testBoolAtom(self):
        ast = parsestring('#t')
        self.assertTrue(bool_p(ast))
        self.assertTrue(ast.val)

    def testDefun(self):
        ast = parsestring("(defun twox (x) (+ x x))")
        self.assertTrue(cell_p(ast))
        self.assertTrue(sym_p(ast.car))
        self.assertEqual(ast.car.val, "defun")
        self.assertEqual(ast.cadr.val, "twox")
        self.assertEqual(ast.caaddr.val, "x")
        self.assertEqual(ast.cadddr.car.val, "+")

    def testQuoteSym(self):
        ast = parsestring("'wibble")
        self.assertTrue(cell_p(ast))
        self.assertTrue(sym_p(ast.car))
        self.assertEqual(ast.car.val, "quote")
        self.assertEqual(ast.cadr.val, "wibble")

    def testQuoteList(self):
        ast = parsestring("'(a b)")
        self.assertTrue(cell_p(ast))
        self.assertTrue(sym_p(ast.car))
        self.assertEqual(ast.car.val, "quote")
        exp = ast.cadr
        self.assertTrue(cell_p(exp))
        self.assertEqual(exp.car.val, "a")
        self.assertTrue(sym_p(exp.cadr))
        self.assertEqual(exp.cadr.val, "b")



class TestEval(unittest.TestCase):
    
    def testIntAtom(self):
        ret = evaluate(parsestring('1'), None)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 1)

    def testStrAtom(self):
        ret = evaluate(parsestring('"str"'), None)
        self.assertTrue(str_p(ret))
        self.assertEqual(ret.val, "str")
    
    def testQuote(self):
        ret = evaluate(parsestring('(quote x)'), None)
        self.assertTrue(sym_p(ret))
        self.assertEqual(ret.val, 'x')

    def testIf(self):
        ret = evaluate(parsestring('(if #t 1 2)'), None)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 1)
        ret = evaluate(parsestring('(if #f 1 2)'), None)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 2)

    def testEmptyBegin(self):
        ret = evaluate(parsestring('(begin)'), None)
        self.assertTrue(cell_p(ret))
        self.assertEqual(ret.car, None)
        self.assertEqual(ret.cdr, None)
        
    def testSingleAtomBegin(self):
        ret = evaluate(parsestring('(begin 2)'), None)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 2)
        
    def testNestedBegin(self):
        ret = evaluate(parsestring('(begin (begin 2))'), None)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 2)

    def testEnvNotFound(self):
        self.assertRaises(LookupError, evaluate, parsestring('x'), None)

    def testSetUndefined(self):
        self.assertRaises(LookupError, evaluate, parsestring('(set! x 10)'), None)


    def testExtendLookup(self):
        env = extend(None, Symbol('x'), Integer(5))
        ret = lookup(Symbol("x"), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)

    def testExtendSingle(self):
        ret = evaluate(parsestring('x'), extend(None, Symbol('x'), Integer(5)))
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)
        
    def testExtendList(self):
        vars = Cell(Symbol("x"), Cell(Symbol("y")))
        vals = Cell(Integer(5), Cell(Integer(10)))
        env = extend(None, vars, vals)
        ret = evaluate(parsestring('x'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)
        ret = evaluate(parsestring('y'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 10)

    def testSet(self):
        vars = Cell(Symbol("x"), Cell(Symbol("y")))
        vals = Cell(Integer(5), Cell(Integer(10)))
        env = extend(None, vars, vals)

        ret = evaluate(parsestring('x'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)

        ret = evaluate(parsestring('(set! x 15)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 15)

        ret = evaluate(parsestring('x'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 15)

    def testLambda(self):
        ret = evaluate(parsestring('(lambda () (quote x))'), None)
        self.assertTrue(fun_p(ret))

    def testLambdaQuote(self):
        ret = evaluate(parsestring('((lambda () (quote x)))'), None)
        self.assertTrue(sym_p(ret))
        self.assertEqual(ret.val, 'x')
        
    def testLambdaReturn(self):
        env = extend(None, Symbol('x'), Integer(5))
        ret = evaluate(parsestring('((lambda () x))'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)

    def testLambdaInvoke(self):
        env = extend(None, Symbol('f'), Integer(0))
        ret = evaluate(parsestring('(begin (set! f (lambda () 5)) (f))'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)

    def testLambdaInvokeArgs(self):
        env = extend(None, Symbol('f'), Integer(0))
        ret = evaluate(parsestring('(begin (set! f (lambda (x) x)) (f 5))'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)

    def testLambdaExtendInvoke(self):
        ret = evaluate(parsestring('((lambda (f) (f)) (lambda () 5))'), None)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)


class TestPrimitives(unittest.TestCase):

    def setUp(self):
        self.env = primitives()
        
    def testPSum2(self):
        lst = Cell(Integer(1), Cell(Integer(1)))
        ret = lisp.do_sum(lst)
        self.assertEqual(ret.val, 2)

    def testPSum3(self):
        lst = Cell(Integer(1), Cell(Integer(1), Cell(Integer(1))))
        ret = lisp.do_sum(lst)
        self.assertEqual(ret.val, 3)
        
    def testSum2(self):
        env = extend(self.env, Symbol('x'), Integer(5))
        ret = evaluate(parsestring('(+ x x)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 10)

    def testSum3(self):
        env = extend(self.env, Symbol('x'), Integer(5))
        ret = evaluate(parsestring('(+ x x x)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 15)

    def testPMult2(self):
        lst = Cell(Integer(2), Cell(Integer(2)))
        ret = lisp.do_mult(lst)
        self.assertEqual(ret.val, 4)

    def testPMult3(self):
        lst = Cell(Integer(3), Cell(Integer(3), Cell(Integer(3))))
        ret = lisp.do_mult(lst)
        self.assertEqual(ret.val, 27)
        
    def testMult2(self):
        env = extend(self.env, Symbol('x'), Integer(5))
        ret = evaluate(parsestring('(* x x)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 25)

    def testMult3(self):
        env = extend(self.env, Symbol('x'), Integer(5))
        ret = evaluate(parsestring('(* x x x)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 125)

        
    def testPSub2(self):
        lst = Cell(Integer(1), Cell(Integer(1)))
        ret = lisp.do_subtract(lst)
        self.assertEqual(ret.val, 0)
        
    def testSub2(self):
        env = extend(self.env, Symbol('x'), Integer(5))
        ret = evaluate(parsestring('(- x x)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 0)

    def testList(self):
        ret = evaluate(parsestring('(list 5 5 5)'), self.env)
        self.assertTrue(ret.car.val, 5)
        self.assertTrue(ret.cadr.val, 5)
        self.assertTrue(ret.caddr.val, 5)

    def testSymList(self):
        env = extend(self.env, Symbol('x'), Integer(5))
        ret = evaluate(parsestring('(list x x x)'), env)
        self.assertTrue(ret.car.val, 5)
        self.assertTrue(ret.cadr.val, 5)
        self.assertTrue(ret.caddr.val, 5)

    def testCar(self):
        ret = evaluate(parsestring('(car (list 1 2 3 4 5))'), self.env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 1)
        
    def testCdr(self):
        ret = evaluate(parsestring('(cdr (list 1 2 3 4 5))'), self.env)
        self.assertTrue(cell_p(ret))
        self.assertEqual(ret.car.val, 2)
        
    def testCadr(self):
        ret = evaluate(parsestring('(cadr (list 1 2 3 4 5))'), self.env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 2)
        
    def testCaar(self):
        ret = evaluate(parsestring('(caar (list (list 1) 2 3 4 5))'), self.env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 1)

    def testIfEqual(self):
        ret = evaluate(parsestring('(if (= 1 2) 1 2)'), self.env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 2)

        env = extend(self.env, Symbol('x'), Integer(5))
        ret = lookup(Symbol("x"), env)
        ret = evaluate(parsestring('(if (= x 2) 1 2)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 2)

        ret = evaluate(parsestring('(if (= x 5) 1 2)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 1)

    def testDefun(self):
        ret, env = tleval(parsestring('(defun five () 5)'), self.env)
        ret = evaluate(parsestring('(five)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)

    def testDefunNestedCall(self):
        ret, env = tleval(parsestring('(defun five () 5)'), self.env)
        ret, env = tleval(parsestring('(defun callfive () (five))'), env)
        ret = evaluate(parsestring('(callfive)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)

    def testDefunQuote(self):
        ret, env = tleval(parsestring('(defun f () (quote x))'), self.env)
        ret = evaluate(parsestring('(f)'), env)        
        self.assertTrue(sym_p(ret))
        self.assertEqual(ret.val, 'x')

    def testDefunSimpleParam(self):
        ret, env = tleval(parsestring('(defun ret (x) x)'), self.env)
        ret = evaluate(parsestring('(ret 5)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)

    def testDefine(self):
        ret, env = tleval(parsestring('(define x 5)'), self.env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)
        ret = evaluate(parsestring('x'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)


    def testSimpleRecursion(self):
        prog = """(defun rfive (n)
                    (if (= n 5)
                       n
                       (rfive (+ 1 n))))"""
        ret, env = tleval(parsestring(prog), self.env)
        ret = evaluate(parsestring('(rfive 0)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 5)

    def testRecursionFib(self):
        ret, env = tleval(parsestring("""(defun fib (n)
                                           (if (< n 2)
                                             n
                                             (+ (fib (- n 1)) (fib (- n 2)))))"""),
                          self.env)
        ret = evaluate(parsestring('(fib 1)'), env)
        self.assertEqual(ret.val, 1)
        ret = evaluate(parsestring('(fib 3)'), env)
        self.assertEqual(ret.val, 2)
        ret = evaluate(parsestring('(fib 5)'), env)
        self.assertEqual(ret.val, 5)
        ret = evaluate(parsestring('(fib 10)'), env)
        self.assertEqual(ret.val, 55)

    def testRecursionFact(self):
        ret, env = tleval(parsestring("""(defun fact (n)
                                           (if (= n 0)
                                             1
                                             (* n (fact (- n 1)))))"""),
                          self.env)
        ret = evaluate(parsestring('(fact 1)'), env)
        self.assertEqual(ret.val, 1)
        ret = evaluate(parsestring('(fact 3)'), env)
        self.assertEqual(ret.val, 6)
        ret = evaluate(parsestring('(fact 5)'), env)
        self.assertEqual(ret.val, 120)
        ret = evaluate(parsestring('(fact 10)'), env)
        self.assertEqual(ret.val, 3628800)

    def testClosureCounter(self):
        exp = """(defun makecounter (x)
                   (lambda ()
                     (set! x (+ 1 x))
                     x))"""
        ret, env = tleval(parsestring(exp), self.env)
        ret, env = tleval(parsestring('(define count (makecounter 0))'), env)
        
        ret = evaluate(parsestring('(count)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 1)
        ret = evaluate(parsestring('(count)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 2)
        ret = evaluate(parsestring('(count)'), env)
        self.assertTrue(int_p(ret))
        self.assertEqual(ret.val, 3)



class TestMacros(unittest.TestCase):

    def setUp(self):
        self.env = primitives()
        self.env = extend(self.env, Symbol('x'), Integer(5))
        self.env = extend(self.env, Symbol('y'), 
                          evaluate(parsestring('(quote (a b c))'), self.env))
        
    def testBackquoteSimple(self):
        # "`basic is the same as 'basic, that is, (quote basic), for
        # any form basic that is not a list or a general vector. -- CLtL
        # 
        ret = evaluate(parsestring('`x'), self.env)
        self.assertTrue(sym_p(ret))
        self.assertEqual(ret.val, "x")

    def testBackquoteUnquoteSimple(self):
        # "`,form is the same as form, for any form, provided that the
        # representation of form does not begin with ``@'' or
        # ``.''. (A similar caveat holds for all occurrences of a form
        # after a comma.)" -- CLtL
        # 
        ret = evaluate(parsestring('`,x'), self.env)
        self.assertEqual(ret.val, 5)

    def testBackquoteInList(self):
        # "The general idea is that the backquote is followed by a
        # template, a picture of a data structure to be built. This
        # template is copied, except that within the template commas
        # can appear. Where a comma occurs, the form following the
        # comma is to be evaluated to produce an object to be inserted
        # at that point." -- CLtL
        # 
        ret = evaluate(parsestring('`(x ,x)'), self.env)
        self.assertTrue(cell_p(ret))
        self.assertTrue(sym_p(ret.car))
        self.assertTrue(int_p(ret.cadr))
        self.assertEqual(ret.cadr.val, 5)

        wanted = evaluate(parsestring("'(x 5)"), self.env)        
        self.assertTrue(equiv(ret, wanted))        

    def testBackquoteDeeper(self):
        ret = evaluate(parsestring('`(x ,x x)'), self.env)
        wanted = evaluate(parsestring("'(x 5 x)"), self.env)        
        self.assertTrue(equiv(ret, wanted))        

    def testBackquoteNestedLists(self):
        ret = evaluate(parsestring('`(x ,x (x ,x))'), self.env)
        wanted = evaluate(parsestring("'(x 5 (x 5))"), self.env)        

        self.assertTrue(equiv(ret, wanted))        

    def testBackquoteSpliceList(self):
        ret = evaluate(parsestring('`(x @y x)'), self.env)
        wanted = evaluate(parsestring("'(x a b c x)"), self.env)       
        self.assertTrue(equiv(ret, wanted))        

    # We don't actually support this currently
    # def testBackquoteSpliceSymbol(self):
    #     ret = evaluate(parsestring('`(x ,x @x)'), self.env)
    #     wanted = evaluate(parsestring("'(x 5 . 5)"), self.env)        

    #     self.assertTrue(equiv(ret, wanted))        

    def testBackquoteSpliceCLTL(self):
        # If a comma is immediately followed by an at-sign (@), then
        # the form following the at-sign is evaluated to produce a
        # list of objects. These objects are then ``spliced'' into
        # place in the template. For example, if x has the value (a b
        # c), then
        #
        # `(x ,x ,@x foo ,(cadr x) bar ,(cdr x) baz ,@(cdr x)) 
        #    => (x (a b c) a b c foo b bar (b c) baz b c)
        # 
        ret = parsestring(
            '`(y ,y @y foo ,(cadr y) bar ,(cdr y) baz @(cdr y))')
        ret = evaluate(ret, self.env)        
        wanted = evaluate(parsestring("'(y (a b c) a b c foo b bar (b c) baz b c)"), 
                          self.env)
        self.assertTrue(equiv(ret, wanted))

    # def testSimple(self):
    #     exp = "(defmacro sqr (x) `(* ,x ,x))"
    #     ret, env = tleval(parsestring(exp), self.env)
    #     ret = evaluate('(sqr 10)', env)
    #     self.assertEqual(ret.val, 100)

    # def testSimpleExpand(self):
    #     exp = "(defmacro sqr (x) `(* ,x ,x))"
    #     ret, env = tleval(parsestring(exp), self.env)
    #     ret = evaluate("(macroexpand-1 '(sqr 10))", env)
    #     wanted = evaluate(parsestring("'(* 10 10)"), env)       
    #     self.assertEqual(equiv(ret, wanted))

    # def testOr(self):
    #     exp = """(defmacro (or a b)
    #               `(let ((temp ,a))
    #                 (if temp
    #                  temp
    #                  ,b))"""
    #     ret, env = tleval(parsestring(exp), self.env)
    #     ret = evaluate('(or #f #t)', env)
    #     # FIXME


if __name__ == '__main__':
    #logging.root.setLevel(logging.DEBUG)
    unittest.main()
