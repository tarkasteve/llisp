from __future__ import print_function

import optparse, logging
import parser, llvmir


######################################################################

if __name__ == '__main__':

    oparse = optparse.OptionParser()
    oparse.add_option("-v", "--verbose", action="store_true",
                      help="Verbose messages", default=False)
    oparse.add_option("-d", "--debug", action="store_true",
                      help="Enable debug output", default=False)
    opts, rest = oparse.parse_args()
    if len(rest) != 1:
        oparse.error("You must supply a file")
       
    if opts.debug:
        logging.root.setLevel(logging.DEBUG)
    elif opts.verbose:
        logging.root.setLevel(logging.INFO)

    # Go ...
    ast = parser.parsefile(rest[0])
    if opts.debug:
        parser.print_ast(ast)

    if opts.debug:
        llvmir.gen(ast)
