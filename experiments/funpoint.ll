; ModuleID = 'cfib.c'

@.str = internal constant [9 x i8] c"Ret: %d\0A\00"		; <[9 x i8]*> [#uses=1]
declare i32 @printf(i8* noalias, ...) nounwind


define i32 @fib(i32 %x) {
entry:
	%cond = icmp sle i32 %x, 2		; <i1> [#uses=1]
	br i1 %cond, label %return, label %recurse

return:		; preds = %entry
	ret i32 1

recurse:		; preds = %entry
	%arg = sub i32 %x, 1		; <i32> [#uses=1]
	%fibx1 = tail call i32 @fib(i32 %arg)		; <i32> [#uses=1]
	%arg1 = sub i32 %x, 2		; <i32> [#uses=1]
	%fibx2 = tail call i32 @fib(i32 %arg1)		; <i32> [#uses=1]
	%addret = add i32 %fibx1, %fibx2		; <i32> [#uses=1]
	ret i32 %addret
}



define i32 @main(i32 %argc, i8** %argv) nounwind {
entry:
	%fp = alloca i32 (i32)*		; <i32 (i32)**> [#uses=2]
	store i32 (i32)* @fib, 
	      i32 (i32)** %fp, align 4

	%0 = load i32 (i32)** %fp, align 4		; <i32 (i32)*> [#uses=1]
	%1 = call i32 %0(i32 44) nounwind		; <i32> [#uses=1]

	%2 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr ([9 x i8]* @.str,
	     	      	    	  	  i32 0, i32 0),
	     	      	    	  	  i32 %1) nounwind		; <i32> [#uses=0]

	ret i32 0
}
