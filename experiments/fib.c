
#include <llvm-c/Core.h>
#include <llvm-c/ExecutionEngine.h>

#include <stdio.h>

int main(int argc, char *argv[])
{
    LLVMModuleRef mod = LLVMModuleCreateWithName("fib");

    LLVMTypeRef i32_t =  LLVMInt32Type();

    LLVMTypeRef params[] = {i32_t};
    LLVMTypeRef ffib_t = LLVMFunctionType(i32_t, params, 1, 0);

    LLVMValueRef ffib = LLVMAddFunction(mod, "fib", ffib_t);
    LLVMValueRef x = LLVMGetParam(ffib, 0);  
    LLVMSetValueName(x, "x");

    LLVMBasicBlockRef entry_b = LLVMAppendBasicBlock(ffib, "entry");
    LLVMBasicBlockRef return_b = LLVMAppendBasicBlock(ffib, "return");
    LLVMBasicBlockRef recurse_b = LLVMAppendBasicBlock(ffib, "recurse");

    LLVMValueRef one = LLVMConstInt(i32_t, 1, 0);
    LLVMValueRef two = LLVMConstInt(i32_t, 2, 0);
    
    LLVMBuilderRef bldr = LLVMCreateBuilder();
    LLVMPositionBuilderAtEnd(bldr, entry_b);

    LLVMValueRef icmp =  LLVMBuildICmp(bldr, LLVMIntSLE, x, two, "cond");
    LLVMValueRef cbr = LLVMBuildCondBr(bldr, icmp, return_b, recurse_b);

    LLVMPositionBuilderAtEnd(bldr, return_b);    
    LLVMValueRef ret = LLVMBuildRet(bldr, one);
    
    LLVMPositionBuilderAtEnd(bldr, recurse_b);    

    LLVMValueRef sub1 = LLVMBuildSub(bldr, x, one, "arg");
    LLVMValueRef c1parms[] = {sub1};
    LLVMValueRef call1 = LLVMBuildCall(bldr, ffib, c1parms, 1, "fibx1");
    LLVMSetTailCall(call1, 1);    

    LLVMValueRef sub2 = LLVMBuildSub(bldr, x, two, "arg");
    LLVMValueRef c2parms[] = {sub2};
    LLVMValueRef call2 = LLVMBuildCall(bldr, ffib, c2parms, 1, "fibx2");
    LLVMSetTailCall(call2, 1);

    LLVMValueRef add =  LLVMBuildAdd(bldr, call1, call2, "addret");
    LLVMValueRef ret2 = LLVMBuildRet(bldr, add);

    
    LLVMDumpModule(mod);


    // JIT

    LLVMModuleProviderRef mp = LLVMCreateModuleProviderForExistingModule(mod);

    LLVMExecutionEngineRef ee;
    char *outmsg;
    int error = LLVMCreateJITCompiler(&ee, mp, 1, &outmsg);
    if (error) {
        printf("GOT ERR: %s\n", outmsg);
        LLVMDisposeMessage(outmsg);
    }
    
    
    LLVMGenericValueRef args[] = {LLVMCreateGenericValueOfInt(i32_t, 44, 0)};
    LLVMGenericValueRef rret = LLVMRunFunction(ee, ffib, 1, args);

    printf("Result: %Ld\n", LLVMGenericValueToInt(rret, 0));

    return 0;
}
