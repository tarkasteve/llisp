; ModuleID = 'fib'

define i32 @fib(i32 %x) {
entry:
	%cond = icmp sle i32 %x, 2		; <i1> [#uses=1]
	br i1 %cond, label %return, label %recurse

return:		; preds = %entry
	ret i32 1

recurse:		; preds = %entry
	%arg = sub i32 %x, 1		; <i32> [#uses=1]
	%fibx1 = tail call i32 @fib(i32 %arg)		; <i32> [#uses=1]
	%arg1 = sub i32 %x, 2		; <i32> [#uses=1]
	%fibx2 = tail call i32 @fib(i32 %arg1)		; <i32> [#uses=1]
	%addret = add i32 %fibx1, %fibx2		; <i32> [#uses=1]
	ret i32 %addret
}

