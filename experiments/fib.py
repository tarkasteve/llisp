
#   int fib(int x) {
#     if(x<=2) return 1;
#     return fib(x-1)+fib(x-2);
#   }

import llvm, llvm.core, llvm.ee


mod = llvm.core.Module.new("fib")

########################################

int32_t = llvm.core.Type.int(32)
fib_sig = llvm.core.Type.function(int32_t, [int32_t])

ffib = mod.add_function(fib_sig, 'fib')
ffib.args[0].name = "x"
x = ffib.args[0]

entry_b = ffib.append_basic_block('entry')
return_b = ffib.append_basic_block('return')
recurse_b = ffib.append_basic_block('recurse')

one = llvm.core.Constant.int(int32_t, 1)
two = llvm.core.Constant.int(int32_t, 2)

bldr = llvm.core.Builder.new (entry_b)

lte = bldr.icmp (llvm.core.ICMP_SLE, x, two, "cond")
bldr.cbranch(lte, return_b, recurse_b)

bldr.position_at_end (return_b)
bldr.ret(one)

bldr.position_at_end (recurse_b)

sub1 = bldr.sub(x, one, 'arg')
call1 = bldr.call(ffib, [sub1], 'fibx1')
call1.tail_call = True

sub2 = bldr.sub(x, two, 'arg')
call2 = bldr.call(ffib, [sub2], 'fibx2')
call2.tail_call = True

addret = bldr.add(call1, call2, 'addret')
bldr.ret(addret)

print mod

########################################

mp = llvm.core.ModuleProvider.new(mod)

ee = llvm.ee.ExecutionEngine.new(mp)

ret = ee.run_function(ffib, [llvm.ee.GenericValue.int(int32_t, 44)])
print ret.as_int()
