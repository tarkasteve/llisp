
#include <stdio.h>

struct thestruct {
    int a;
    int b;
};

void foo(struct thestruct passedstruct)
{
    printf("%d\n", passedstruct.a);
}

int main(int argc, char *argv[])
{
    struct thestruct mystruct = {1, 2};
    foo(mystruct);

}
