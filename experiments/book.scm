
(use syntax-case)
(use test)


(test-begin "Book tests")

;;;;;;;;;;;;;;;;;;;; 1.3 ;;;;;;;;;;;;;;;;;;;;

(define (lookup exp env)
  (error "not implemented"))

(define (evaluate1.3 exp env)
  (if (atom? exp)
      (cond ((symbol? exp) (lookup exp env))
	    ((or (number? exp) (string? exp) (char? exp) (boolean? exp) (vector? exp))
	     exp)
	    (else (error "Unknown type")))))

(test-group "evaluate1.3"
            (test "test" (evaluate1.3 "test" '()))
            (test 123 (evaluate1.3 123 '()))
            (test #\x (evaluate1.3 #\x '()))
            (test #t (evaluate1.3 #t '()))
            (test-error (evaluate1.3 'test '()))
            (test-error (evaluate1.3 '() '())))


;;;;;;;;;;;;;;;;;;;; 1.4 ;;;;;;;;;;;;;;;;;;;;

(define (update!1.4) (error "not implemented"))
(define (make-function1.4) (error "not implemented"))
(define (invoke1.4) (error "not implemented"))

(define (eprogn1.4 exps env)
  (if (pair? exps)
      ; Yes: Has contents
      (if (pair? (cdr exps))
          ; Yes: More than one; eval first, recurse over rest
          (begin
            (evaluate1.4 (car exps) env)
            (eprogn1.4 (cdr exps) env))
          ; No: Final; evaluate to return value
          (evaluate1.4 (car exps) env))
      ; No: Empty
      '()))
          
(define (evallist1.4 exps env)
  (if (pair? exps)
      (cons (evaluate1.4 (car exps) env)
            (evallist1.4 (cdr exps) env))
      '()))

(define (evaluate1.4 exp env)
  (if (atom? exp)
      ;; Atoms; numbers, strings, symbols, etc
      (cond ((symbol? exp) (lookup exp env))
	    ((or (number? exp) (string? exp) (char? exp) (boolean? exp) (vector? exp))
	     exp)
	    (else (error "Unknown type")))

      ;; Start of form
      (case (car exp)
        ((quote)  (cadr exp))
        ((if)     (if (evaluate1.4 (cadr exp) env)
                      (evaluate1.4 (caddr exp) env)
                      (evaluate1.4 (cadddr exp) env)))
        ((begin)  (eprogn1.4 (cdr exp) env))
        ((set!)   (update!1.4 (cadr exp) env
                              (evaluate1.4 (caddr exp) env)))
        ((lambda) (make-function1.4 (cadr exp) (cddr exp) env))
        (else (invoke1.4 (evaluate1.4 (car exp) env)
                      (evallist1.4 (cdr exp) env))))))

(test-group "evaluate1.4"
            (test 'x (evaluate1.4 '(quote x) '()))
            (test 2 (evaluate1.4 '(if #f 1 2) '()))
            (test '() (evaluate1.4 '(begin) '()))
            (test '2 (evaluate1.4 '(begin 2) '()))
            (test '2 (evaluate1.4 '(begin (begin 2)) '()))
            (test '(1 2 3) (evallist1.4 '(1 2 3) '()))
            )

;;;;;;;;;;;;;;;;;;;; 1.5 ;;;;;;;;;;;;;;;;;;;;

(define (make-function1.5) (error "not implemented"))
(define (invoke1.5) (error "not implemented"))

(define (extend1.5 env vars vals)
  (cond ((pair? vars)
         (if (pair? vals)
             (cons (cons (car vars) (car vals))
                   (extend1.5 env (cdr vars) (cdr vals)))
             (error "Not enough values in extend")))
        ((null? vars)
         (if (null? vals)
             env ; Done
             (error "Too many values in extend")))
        ((symbol? vars)
         (cons (cons vars vals) env))))

(define (update!1.5 id env value)
  (if (pair? env)
      (if (eq? (caar env) id)
          (begin
            (set-cdr! (car env) value)
            value)
          (update!1.5 id (cdr env) value))
      (error "Binding not found in update")))

(define (lookup1.5 id env)
  (if (pair? env)
      (if (eq? (caar env) id)
          (cdar env)
          (lookup1.5 id (cdr env)))
      (error "Binding not found in lookup")))

(define (eprogn1.5 exps env)
  (if (pair? exps)
      ; Yes: Has contents
      (if (pair? (cdr exps))
          ; Yes: More than one; eval first, recurse over rest
          (begin
            (evaluate1.5 (car exps) env)
            (eprogn1.5 (cdr exps) env))
          ; No: Final; evaluate to return value
          (evaluate1.5 (car exps) env))
      ; No: Empty
      '()))
          
(define (evallist1.5 exps env)
  (if (pair? exps)
      (cons (evaluate1.5 (car exps) env)
            (evallist1.5 (cdr exps) env))
      '()))

(define (evaluate1.5 exp env)
  (if (atom? exp)
      ;; Atoms; numbers, strings, symbols, etc
      (cond ((symbol? exp) (lookup1.5 exp env))
	    ((or (number? exp) (string? exp) (char? exp) (boolean? exp) (vector? exp))
	     exp)
	    (else (error "Unknown type")))

      ;; Start of form
      (case (car exp)
        ((quote)  (cadr exp))
        ((if)     (if (evaluate1.5 (cadr exp) env)
                      (evaluate1.5 (caddr exp) env)
                      (evaluate1.5 (cadddr exp) env)))
        ((begin)  (eprogn1.5 (cdr exp) env))
        ((set!)   (update!1.5 (cadr exp) env
                              (evaluate1.5 (caddr exp) env)))
        ((lambda) (make-function1.5 (cadr exp) (cddr exp) env))
        (else (invoke1.5 (evaluate1.5 (car exp) env)
                      (evallist1.5 (cdr exp) env))))))

(test-group "evaluate1.5"
            (let ((env '()))
              (test-error (evaluate1.5 'x env))
              (test-error (evaluate1.5 '(set! x 10) env))
              (test 5 (evaluate1.5 'x
                                   (extend1.5 env 'x 5)))
              (test-error (evaluate1.5 'x env))
              (test 5 (evaluate1.5 'x
                                   (extend1.5 env '(x y) '(5 10))))
              (test 10 (evaluate1.5 'y
                                   (extend1.5 env '(x y) '(5 10))))
              (test 15 (evaluate1.5 '(set! x 15)
                                    (extend1.5 env 'x 5)))
              (test '(5 10) (evallist1.5 '(x y)
                                         (extend1.5 env '(x y) '(5 10))))
              (test '30 (evaluate1.5 '(begin
                                        (set! x 20)
                                        (set! y 30)
                                        y)
                                     (extend1.5 env '(x y) '(5 10))))
	      (test (list (cons 'x 1))
		    (extend1.5 '() 'x 1))
	      (test (list (cons 'y 1))
		    (let ((e '()))
		      (begin
			(extend1.5 e 'x 1)
			(extend1.5 e 'y 1))))
	      (test (list (cons 'y 2) (cons 'x 1))
		    (extend1.5 
		     (extend1.5 '() 'x 1)
		     'y 2))
	      (test (list (cons 'x 3) (cons 'y 2) (cons 'x 1))
		    (extend1.5
		     (extend1.5 
		      (extend1.5 '() 'x 1)
		      'y 2) 'x 3))
	      (test 3 (lookup1.5 'x (extend1.5
				     (extend1.5 '() 'x 1)
				     'x 3)))
              ))


;;;;;;;;;;;;;;;;;;;; 1.6 ;;;;;;;;;;;;;;;;;;;;

;(set! env.init1.6 '())

(define (make-function1.6 vars body env)
  (lambda (values)
    (eprogn1.6 body (extend1.6 env vars values))))

(define (invoke1.6 fn args)
  (if (procedure? fn)
      (fn args)
      (error "Non-procedure call in invoke")))

(define (extend1.6 env vars vals)
  (cond ((pair? vars)
         (if (pair? vals)
             (cons (cons (car vars) (car vals))
                   (extend1.6 env (cdr vars) (cdr vals)))
             (error "Not enough values in extend")))
        ((null? vars)
         (if (null? vals)
             env ; Done
             (error "Too many values in extend")))
        ((symbol? vars)
         (cons (cons vars vals) env))))

(define (update!1.6 id env value)
  (if (pair? env)
      (if (eq? (caar env) id)
          (begin
            (set-cdr! (car env) value)
            value)
          (update!1.6 id (cdr env) value))
      (error "Binding " id " not found in update")))

(define (lookup1.6 id env)
  (if (pair? env)
      (if (eq? (caar env) id)
          (cdar env)
          (lookup1.6 id (cdr env)))
      (error "Binding " id " not found in lookup")))

(define (eprogn1.6 exps env)
  (if (pair? exps)
      ; Yes: Has contents
      (if (pair? (cdr exps))
          ; Yes: More than one; eval first, recurse over rest
          (begin
            (evaluate1.6 (car exps) env)
            (eprogn1.6 (cdr exps) env))
          ; No: Final; evaluate to return value
          (evaluate1.6 (car exps) env))
      ; No: Empty
      '()))
          
(define (evallist1.6 exps env)
  (if (pair? exps)
      (cons (evaluate1.6 (car exps) env)
            (evallist1.6 (cdr exps) env))
      '()))

(define (evaluate1.6 exp env)
  (if (atom? exp)
      ;; Atoms; numbers, strings, symbols, etc
      (cond ((symbol? exp) (lookup1.6 exp env))
	    ((or (number? exp) (string? exp) (char? exp) (boolean? exp) (vector? exp))
	     exp)
	    (else (error "Unknown type")))

      ;; Start of form
      (case (car exp)
        ((@quote)  (cadr exp))
        ((@if)     (if (evaluate1.6 (cadr exp) env)
                      (evaluate1.6 (caddr exp) env)
                      (evaluate1.6 (cadddr exp) env)))
        ((@begin)  (eprogn1.6 (cdr exp) env))
        ((@set!)   (update!1.6 (cadr exp) env
                              (evaluate1.6 (caddr exp) env)))
        ((@lambda) (make-function1.6 (cadr exp) (cddr exp) env))
        (else (invoke1.6 (evaluate1.6 (car exp) env)
                      (evallist1.6 (cdr exp) env))))))


(test-group "evaluate1.6"
            (let ((env '()))
              (test 'x (evaluate1.6 '((@lambda () (@quote x))) env))
              (test 1 (evaluate1.6 '((@lambda () x))
                                   (extend1.5 env 'x 1)))
              (test 1 (evaluate1.6 '(@begin
                                      (@set! fn (@lambda (x) x))
                                      (fn 1))
                                   (extend1.5 env 'fn 0)))
              (test '(a b) (evaluate1.6 '(@quote (a b))
                                        env))
              (test '(a b) (evaluate1.6 '((@lambda () (@quote (a b))))
                                        env))
	      ;; Need list to test
              ;; (test (list 1 2) (evaluate1.6 '((@lambda (vals) vals) 1)
	      ;; 				    env))
              ;; (test (list 1 2) (evaluate1.6 '(((@lambda (a)
	      ;; 					 (@lambda (b)
	      ;; 					   ((@lambda (vals) vals)) a b)) 1) 2)
	      ;; 				    env))
              ;; (test (list 1 2) (evaluate1.6 '(((@lambda (a)
	      ;; 					 (@lambda (b)
	      ;; 					   ((@lambda (vals) vals)) a b)) 1) 2)
	      ;; 				    env))
              ))



;;;;;;;;;;;;;;;;;;;; 1.7 ;;;;;;;;;;;;;;;;;;;;

(set! env.init1.7 '())
(set! env.global1.7 '())

(define (make-function1.7 vars body env)
  (lambda (values)
    (eprogn1.7 body (extend1.7 env vars values))))

(define (invoke1.7 fn args)
  (if (procedure? fn)
      (fn args)
      (error "Non-procedure call in invoke")))

(define (extend1.7 env vars vals)
  (cond ((pair? vars)
         (if (pair? vals)
             (cons (cons (car vars) (car vals))
                   (extend1.7 env (cdr vars) (cdr vals)))
             (error "Not enough values in extend")))
        ((null? vars)
         (if (null? vals)
             env ; Done
             (error "Too many values in extend")))
        ((symbol? vars)
         (cons (cons vars vals) env))))

(define (update!1.7 id env value)
  (if (pair? env)
      (if (eq? (caar env) id)
          (begin
            (set-cdr! (car env) value)
            value)
          (update!1.7 id (cdr env) value))
      (error "Binding " id " not found in update")))

(define (lookup1.7 id env)
  (if (pair? env)
      (if (eq? (caar env) id)
          (cdar env)
          (lookup1.7 id (cdr env)))
      (error "Binding " id " not found in lookup")))

(define (eprogn1.7 exps env)
  (if (pair? exps)
      ; Yes: Has contents
      (if (pair? (cdr exps))
          ; Yes: More than one; eval first, recurse over rest
          (begin
            (evaluate1.7 (car exps) env)
            (eprogn1.7 (cdr exps) env))
          ; No: Final; evaluate to return value
          (evaluate1.7 (car exps) env))
      ; No: Empty
      '()))
          
(define (evallist1.7 exps env)
  (if (pair? exps)
      (cons (evaluate1.7 (car exps) env)
            (evallist1.7 (cdr exps) env))
      '()))

(define (evaluate1.7 exp env)
  (if (atom? exp)
      ;; Atoms; numbers, strings, symbols, etc
      (cond ((symbol? exp) (lookup1.7 exp env))
	    ((or (number? exp) (string? exp) (char? exp) (boolean? exp) (vector? exp))
	     exp)
	    (else (error "Unknown type")))

      ;; Start of form
      (case (car exp)
        ((@quote)  (cadr exp))
        ((@if)     (if (evaluate1.7 (cadr exp) env)
                      (evaluate1.7 (caddr exp) env)
                      (evaluate1.7 (cadddr exp) env)))
        ((@begin)  (eprogn1.7 (cdr exp) env))
        ((@set!)   (update!1.7 (cadr exp) env
                              (evaluate1.7 (caddr exp) env)))
        ((@lambda) (make-function1.7 (cadr exp) (cddr exp) env))
        (else (invoke1.7 (evaluate1.7 (car exp) env)
                      (evallist1.7 (cdr exp) env))))))

(define-syntax definitial1.7 
  (syntax-rules ()
    ((definitial1.7 name)
     (begin (set! env.global1.7 (cons (cons 'name 'void) env.global1.7))
            'name ) )
    ((definitial1.7 name value)
     (begin (set! env.global1.7 (cons (cons 'name value) env.global1.7))
            'name ) ) ) )

(define-syntax defprimitive1.7
  (syntax-rules ()
    ((defprimitive1.7 name value arity)
     (definitial1.7 name 
       (lambda (values) 
	 (if (= arity (length values))
	     (apply value values)       ; The real apply of Scheme
	     (wrong "Incorrect arity"
		    (list 'name values) ) ) ) ) ) ) )

;(definitial @t #t)
;(definitial @f (gensym))
;(definitial @nil '())

(defprimitive1.7 @cons cons 2)
(defprimitive1.7 @car car 1)
(defprimitive1.7 @set-cdr! set-cdr 2)
(defprimitive1.7 @+ + 2)
(defprimitive1.7 @eq? eq? 2)
(defprimitive1.7 @< < 2)

(test-group "evaluate1.7"
	    (test '3 (evaluate1.7 '(@+ 1 2) env.global1.7))
	    (test #t (evaluate1.7 '(@eq? 1 1) env.global1.7))
	    (test #f (evaluate1.7 '(@eq? 1 2) env.global1.7))
	    (test #t (evaluate1.7 '(@< 1 2) env.global1.7))
	    (test 5 (evaluate1.7 '((((@lambda (a)
				       (@lambda (a) 
					 (@lambda (b)
					   (@+ a b)))) 1) 2) 3)
				 env.global1.7)))


;;;;;;;;;;;;;;;;;;;; 1.10.1 ;;;;;;;;;;;;;;;;;;;;

(set! env.init1.10.1 '())
(set! env.global1.10.1 '())

(define (make-function1.10.1 vars body env)
  (lambda (values)
    (eprogn1.10.1 body (extend1.10.1 env vars values))))

(define (invoke1.10.1 fn args)
  (if (procedure? fn)
      (begin
	(print fn args)
	(fn args))
      (error "Non-procedure call in invoke")))

(define (extend1.10.1 env vars vals)
  (cond ((pair? vars)
         (if (pair? vals)
             (cons (cons (car vars) (car vals))
                   (extend1.10.1 env (cdr vars) (cdr vals)))
             (error "Not enough values in extend")))
        ((null? vars)
         (if (null? vals)
             env ; Done
             (error "Too many values in extend")))
        ((symbol? vars)
         (cons (cons vars vals) env))))

(define (update!1.10.1 id env value)
  (if (pair? env)
      (if (eq? (caar env) id)
          (begin
            (set-cdr! (car env) value)
            value)
          (update!1.10.1 id (cdr env) value))
      (error "Binding " id " not found in update")))

(define (lookup1.10.1 id env)
  (if (pair? env)
      (if (eq? (caar env) id)
          (cdar env)
          (lookup1.10.1 id (cdr env)))
      (error "Binding " id " not found in lookup")))

(define (eprogn1.10.1 exps env)
  (if (pair? exps)
      ; Yes: Has contents
      (if (pair? (cdr exps))
          ; Yes: More than one; eval first, recurse over rest
          (begin
            (evaluate1.10.1 (car exps) env)
            (eprogn1.10.1 (cdr exps) env))
          ; No: Final; evaluate to return value
          (evaluate1.10.1 (car exps) env))
      ; No: Empty
      '()))
          
(define (evallist1.10.1 exps env)
  (if (pair? exps)
      (cons (evaluate1.10.1 (car exps) env)
            (evallist1.10.1 (cdr exps) env))
      '()))

(define (evaluate1.10.1 exp env)
  (if (atom? exp)
      ;; Atoms; numbers, strings, symbols, etc
      (cond ((symbol? exp) (lookup1.10.1 exp env))
	    ((or (number? exp) (string? exp) (char? exp) (boolean? exp) (vector? exp))
	     exp)
	    (else (error "Unknown type")))

      ;; Start of form
      (case (car exp)
        ((@quote)  (cadr exp))
        ((@if)     (if (evaluate1.10.1 (cadr exp) env)
                      (evaluate1.10.1 (caddr exp) env)
                      (evaluate1.10.1 (cadddr exp) env)))
        ((@begin)  (eprogn1.10.1 (cdr exp) env))
        ((@set!)   (update!1.10.1 (cadr exp) env
                              (evaluate1.10.1 (caddr exp) env)))
        ((@lambda) (make-function1.10.1 (cadr exp) (cddr exp) env))
        (else (invoke1.10.1 (evaluate1.10.1 (car exp) env)
                      (evallist1.10.1 (cdr exp) env))))))

(define-syntax definitial1.10.1 
  (syntax-rules ()
    ((definitial1.10.1 name)
     (begin (set! env.global1.10.1 (cons (cons 'name 'void) env.global1.10.1))
            'name ) )
    ((definitial1.10.1 name value)
     (begin (set! env.global1.10.1 (cons (cons 'name value) env.global1.10.1))
            'name ) ) ) )

(define-syntax defprimitive1.10.1
  (syntax-rules ()
    ((defprimitive1.10.1 name value arity)
     (definitial1.10.1 name 
       (lambda (values) 
	 (if (= arity (length values))
	     (apply value values)       ; The real apply of Scheme
	     (wrong "Incorrect arity"
		    (list 'name values) ) ) ) ) ) ) )

;(definitial @t #t)
;(definitial @f (gensym))
;(definitial @nil '())

(defprimitive1.10.1 @cons cons 2)
(defprimitive1.10.1 @car car 1)
(defprimitive1.10.1 @set-cdr! set-cdr 2)
(defprimitive1.10.1 @+ + 2)
(defprimitive1.10.1 @eq? eq? 2)
(defprimitive1.10.1 @< < 2)

(test-group "evaluate1.10.1"
	    (test '3 (evaluate1.10.1 '(@+ 1 2)
				     env.global1.10.1))
	    (test #t (evaluate1.10.1 '(@eq? 1 1) env.global1.10.1))
	    (test #f (evaluate1.10.1 '(@eq? 1 2) env.global1.10.1))
	    (test #t (evaluate1.10.1 '(@< 1 2) env.global1.10.1))
	    (test 5 (evaluate1.10.1 '((((@lambda (a)
				       (@lambda (a) 
					 (@lambda (b)
					   (@+ a b)))) 1) 2) 3)
				 env.global1.10.1)))


;;;;;;;;;;;;;;;;;;;; 1.10.6 ;;;;;;;;;;;;;;;;;;;;

(set! env.init1.10.6 '())
(set! env.global1.10.6 '())

(define (make-function1.10.6 vars body env)
  (lambda (values)
    (eprogn1.10.6 body (extend1.10.6 env vars values))))

(define (invoke1.10.6 fn args)
  (if (procedure? fn)
      (fn args)
      (error "Non-procedure call in invoke")))

(define (extend1.10.6 env vars vals)
  (cond ((pair? vars)
         (if (pair? vals)
             (cons (cons (car vars) (car vals))
                   (extend1.10.6 env (cdr vars) (cdr vals)))
             (error "Not enough values in extend")))
        ((null? vars)
         (if (null? vals)
             env ; Done
             (error "Too many values in extend")))
        ((symbol? vars)
         (cons (cons vars vals) env))))

(define (update!1.10.6 id env value)
  (if (pair? env)
      (if (eq? (caar env) id)
          (begin
            (set-cdr! (car env) value)
            value)
          (update!1.10.6 id (cdr env) value))
      (error "Binding " id " not found in update")))

(define (lookup1.10.6 id env)
  (if (pair? env)
      (if (eq? (caar env) id)
          (cdar env)
          (lookup1.10.6 id (cdr env)))
      (error "Binding " id " not found in lookup")))

(define (eprogn1.10.6 exps env)
  (if (pair? exps)
      ; Yes: Has contents
      (if (pair? (cdr exps))
          ; Yes: More than one; eval first, recurse over rest
          (begin
            (evaluate1.10.6 (car exps) env)
            (eprogn1.10.6 (cdr exps) env))
          ; No: Final; evaluate to return value
          (evaluate1.10.6 (car exps) env))
      ; No: Empty
      '()))
          
(define (evallist1.10.6 exps env)
  (if (pair? exps)
      (cons (evaluate1.10.6 (car exps) env)
            (evallist1.10.6 (cdr exps) env))
      '()))

(define (evaluate1.10.6 exp env)
  (if (atom? exp)
      ;; Atoms; numbers, strings, symbols, etc
      (cond ((symbol? exp) (lookup1.10.6 exp env))
	    ((or (number? exp) (string? exp) (char? exp) (boolean? exp) (vector? exp))
	     exp)
	    (else (error "Unknown type")))

      ;; Start of form
      (case (car exp)
        ((@quote)  (cadr exp))
        ((@if)     (if (evaluate1.10.6 (cadr exp) env)
                      (evaluate1.10.6 (caddr exp) env)
                      (evaluate1.10.6 (cadddr exp) env)))
        ((@begin)  (eprogn1.10.6 (cdr exp) env))
        ((@set!)   (update!1.10.6 (cadr exp) env
                              (evaluate1.10.6 (caddr exp) env)))
        ((@lambda) (make-function1.10.6 (cadr exp) (cddr exp) env))
        (else (invoke1.10.6 (evaluate1.10.6 (car exp) env)
                      (evallist1.10.6 (cdr exp) env))))))

(define-syntax definitial1.10.6 
  (syntax-rules ()
    ((definitial1.10.6 name)
     (begin (set! env.global1.10.6 (cons (cons 'name 'void) env.global1.10.6))
            'name ) )
    ((definitial1.10.6 name value)
     (begin (set! env.global1.10.6 (cons (cons 'name value) env.global1.10.6))
            'name ) ) ) )

(define-syntax defprimitive1.10.6
  (syntax-rules ()
    ((defprimitive1.10.6 name value arity)
     (definitial1.10.6 name 
       (lambda (values) 
	 (if (= arity (length values))
	     (apply value values)       ; The real apply of Scheme
	     (wrong "Incorrect arity"
		    (list 'name values) ) ) ) ) ) ) )

;(definitial @t #t)
;(definitial @f (gensym))
;(definitial @nil '())

(defprimitive1.10.6 @cons cons 2)
(defprimitive1.10.6 @car car 1)
(defprimitive1.10.6 @set-cdr! set-cdr 2)
(defprimitive1.10.6 @+ + 2)
(defprimitive1.10.6 @eq? eq? 2)
(defprimitive1.10.6 @< < 2)
(defprimitive1.10.6 @< < 2)

(definitial1.10.6 @list
  (lambda (vals) vals))

(test-group "evaluate1.10.6"
	    (test '3 (evaluate1.10.6 '(@+ 1 2)
				     env.global1.10.6))
	    (test #t (evaluate1.10.6 '(@eq? 1 1) env.global1.10.6))
	    (test #f (evaluate1.10.6 '(@eq? 1 2) env.global1.10.6))
	    (test #t (evaluate1.10.6 '(@< 1 2) env.global1.10.6))
	    (test (list 1 2)
		  (evaluate1.10.6 '(@list 1 2) env.global1.10.6))
	    (test 5 (evaluate1.10.6 '((((@lambda (a)
				       (@lambda (a) 
					 (@lambda (b)
					   (@+ a b)))) 1) 2) 3)
				 env.global1.10.6)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(test-end)
