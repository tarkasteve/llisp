
gcc -c `llvm-config --cflags` \
    fib.c -o fib.o

g++ `llvm-config --ldflags` \
    `llvm-config --libs core analysis scalaropts executionengine jit  native interpreter bitreader bitwriter instrumentation ipa ipo transformutils asmparser linker support` \
    -fPIC \
    fib.o -o fib \
    `llvm-config --libdir`/libLLVMCore.a


#  gcc -pthread -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -fPIC -D__STDC_CONSTANT_MACROS -D__STDC_LIMIT_MACROS -D_GNU_SOURCE -I/opt/llvm/include -I/usr/include/python2.5 -c fib.c -o fib.o

# g++ -pthread -shared -Wl,-O1  fib.o /opt/llvm/lib/LLVMInterpreter.o /opt/llvm/lib/LLVMX86AsmPrinter.o /opt/llvm/lib/LLVMX86CodeGen.o /opt/llvm/lib/LLVMExecutionEngine.o /opt/llvm/lib/LLVMJIT.o -L/opt/llvm/lib -lpthread -lm -lstdc++ -ldl -lLLVMLinker -lLLVMArchive -lLLVMAsmParser -lLLVMipo -lLLVMInstrumentation -lLLVMBitWriter -lLLVMBitReader -lLLVMSelectionDAG -lLLVMAsmPrinter -lLLVMCodeGen -lLLVMScalarOpts -lLLVMTransformUtils -lLLVMipa -lLLVMAnalysis -lLLVMTarget -lLLVMCore -lLLVMSupport -lLLVMSystem -o fib -fPIC


#  gcc -pthread -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -fPIC -D__STDC_CONSTANT_MACROS -D__STDC_LIMIT_MACROS -D_GNU_SOURCE -I/opt/llvm/include -I/usr/include/python2.5 -c fac.c -o fac.o

# g++ -pthread -shared -Wl,-O1 fac.o /opt/llvm/lib/LLVMInterpreter.o /opt/llvm/lib/LLVMX86AsmPrinter.o /opt/llvm/lib/LLVMX86CodeGen.o /opt/llvm/lib/LLVMExecutionEngine.o /opt/llvm/lib/LLVMJIT.o -L/opt/llvm/lib -lpthread -lm -lstdc++ -ldl -lLLVMLinker -lLLVMArchive -lLLVMAsmParser -lLLVMipo -lLLVMInstrumentation -lLLVMBitWriter -lLLVMBitReader -lLLVMSelectionDAG -lLLVMAsmPrinter -lLLVMCodeGen -lLLVMScalarOpts -lLLVMTransformUtils -lLLVMipa -lLLVMAnalysis -lLLVMTarget -lLLVMCore -lLLVMSupport -lLLVMSystem -o fac -fPIC
