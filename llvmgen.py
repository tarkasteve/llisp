from __future__ import print_function

import llvm, llvm.core, llvm.ee
from parser import *

i32_t = llvm.core.Type.int(32)
var_t = llvm.core.Type.struct([i32_t])
defun_t = llvm.core.Type.function(var_t, [var_t])

def gen(ast):
    mod = llvm.core.Module.new("llisp")

    rgen(ast, mod)

    print(mod)


def rgen(ast, mod):
    for node in ast.list:
        if isinstance(node, Symbol) and node.val == "defun":
            print("!! Defun !!")
            gen_defun(node, mod)

        elif isinstance(node, List):
            print(node.__class__)
            rgen(node, mod)

        else:
            print(node.__class__, "=", node.val)


def gen_defun(node, mod):
    fun = mod.add_function(defun_t, 'defun')
    entry_b = fun.append_basic_block('entry')
